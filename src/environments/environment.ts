// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api: {
    base: 'https://api.nasa.gov/',
    key: 'api_key=8S8pS7X49ZHZFrH5OxDsSVGLkbzLUruf4V78O9sh',
    imageLink: 'planetary/apod',
    asteroids: '/neo/rest/v1/feed?',
    startdate: 'start_date=',
    enddate: 'end_date='
  }
};
