import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Component({
  selector: 'app-asteroids',
  templateUrl: './asteroids.component.html',
  styleUrls: ['./asteroids.component.css']
})
export class AsteroidsComponent implements OnInit {
  public asteroids: any;
  public asteroidsArr: Array<object> = [];
  public asteroidsArrX: Array<object> = [];
  public startDate: any;
  public endDate: any;
  public fd: string;
  public td: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {

  }

  getAsteroids() {

    if ((this.endDate.day - this.startDate.day) > 7) {
      alert('The mentioned periode is greather then 7 days.!!');
      return;
    } else {

      console.log(this.startDate, this.endDate);
      this.fd = this.startDate.year + '-' + this.startDate.month + '-' + this.startDate.day;
      console.log('>>>>>>>>>>>>>>>>>>>>>   ', this.fd);
      this.td = this.endDate.year + '-' + this.endDate.month + '-' + this.endDate.day;
      console.log('<<<<<<<<<<<<<<<<<<<<<<   ', this.td);

      return this.http.get(environment.api.base +
        environment.api.asteroids +
        environment.api.startdate +
        this.fd +
        '&' +
        environment.api.enddate +
        this.td +
        '&' +
        environment.api.key
      ).toPromise().then(data => {
        this.asteroids = data;
        console.log('- - - - - - - - - - ', this.asteroids);
        this.convertArray(this.asteroids.near_earth_objects);
      }).catch(err => {
        console.log(err);
      });

    }

  }

  convertArray(ast: any) {
    this.asteroidsArr = [];
    const arr = Object.keys(ast);
    arr.forEach(key => {
      this.asteroidsArr = this.asteroidsArr.concat(ast[key]);
    });
    console.log('Arr', this.asteroidsArr);

    return this.asteroidsArr;
  }

}
