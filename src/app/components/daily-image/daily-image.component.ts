
import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';



@Component({
  selector: 'app-daily-image',
  templateUrl: './daily-image.component.html',
  styleUrls: ['./daily-image.component.css']
})
export class DailyImageComponent implements OnInit {

  public dailyImageData: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    return this.http.get(environment.api.base + environment.api.imageLink + '?' + environment.api.key)
      .toPromise().then(data => {
        this.dailyImageData = data;
        console.log('full json', this.dailyImageData);
        console.log('image!!!!!!!!!!!!!!!!', this.dailyImageData.url);
      }).catch(err => {
        console.log(err);
      });
  }

}
