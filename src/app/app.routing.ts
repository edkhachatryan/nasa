import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {AsteroidsComponent} from './components/asteroids/asteroids.component';
import {DailyImageComponent} from './components/daily-image/daily-image.component';
import {UfoComponent} from './components/ufo/ufo.component';


const routes: Routes = [
  { path: 'home',             component: HomeComponent },
  { path: 'asteroids',             component: AsteroidsComponent },
  { path: 'daily-image',             component: DailyImageComponent },
  { path: 'ufo',             component: UfoComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: false
    })
  ],
  exports: [
  ]
})
export class AppRoutingModule { }
